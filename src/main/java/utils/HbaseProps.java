package utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * hbase 基本属性配置
 * @author ymy
 */
@Getter
@Setter
public final class HbaseProps implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static HbaseProps build() {
		return new HbaseProps();
	}
	
	/**
	 * zookeeper地址
	 */
	private String quorum = "19.15.59.168,19.15.59.169,19.15.59.170,19.15.59.171,19.15.59.172";
//	private String quorum = "tbds-19-15-59-169,tbds-19-15-59-168,tbds-19-15-59-170,tbds-19-15-59-172,tbds-19-15-59-171";
	/**
	 * 端口
	 */
	private String clientPort = "2181";
	/**
	 * 超时
	 */
	private String period = "6000000";
	/**
	 * 表空间
	 */
	private String namespace = "dataMapping";
	/**
	 * base secureid
	 */
	private String hbase_tbds_secureid = "akw4vkjDSkLlPpKGYc3jchIIs2jCaWvYmKzs";
	/**
	 * hbase securekey
	 */
	private String hbase_tbds_securekey = "7fQ9uSl7MAaAQit16R25Baj7Hjnk7fuY";
	/**
	 * hadoop secureid
	 */
	private String hadoop_tbds_secureid = "Z2qJYgdVxatcWOAzOOChrc6QhD2UPv93TC4x";
	/**
	 * hadoop securekey
	 */
	private String hadoop_tbds_securekey = "qf0osc2m7SSIvzSdciMcYG918l1ae4qu";
	/**
	 * 是否限制字节数
	 */
	private String maxSize = "-1";
	/**
	 * socket超时实际
	 */
	private String socketTimeout = "360000";
	/**
	 * hbase、hadoop 配置
	 */
	private String[] hadoop = new String[] {
			"/usr/hdp/current/hbase-client/conf/core-site.xml",
			"/usr/hdp/current/hbase-client/conf/hbase-site.xml"
	};
}
