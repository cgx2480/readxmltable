package com.outlook.chenxu;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import utils.HbaseProps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppMain {
    private static final byte[] FAMILY_NAME = Bytes.toBytes("info");

    public static void main(String[] args) throws IOException, DocumentException {
        Configuration conf = getConfiguration();
        Connection connection = ConnectionFactory.createConnection(conf);
        Table xmlTable = connection.getTable(TableName.valueOf("dataMapping:zcx_nature_legal_xml"));
        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);
        ResultScanner results = xmlTable.getScanner(scan);

        List<String> cmds = new ArrayList<>();
        for (Result result : results) {
            String rowkey = Bytes.toString(result.getRow());
            String xml = Bytes.toString(result.getValue(FAMILY_NAME, Bytes.toBytes("xml")));
            String mold = Bytes.toString(result.getValue(FAMILY_NAME, Bytes.toBytes("mold")));
            String sourceTableName = getSourceTableName(xml);
//            cmds.add("hadoop jar /gd/nlpson-spark/SparkReadHbase.jar " + mold + " " + getSourceTableName + " " + xml);
            if (mold.equals("1") || mold.equals("4")) {
                cmds.add("hadoop jar /gd/nlpson-spark/SparkReadHbase.jar com.outlook.chenxu.AppMainMR " + mold + " " + sourceTableName + " " + rowkey);
            }
        }

        cmds.forEach(System.out::println);
        cmds.forEach(AppMain::executeOrder);

    }

    private static String getSourceTableName(String xml) throws DocumentException {
        Document document = DocumentHelper.parseText(xml);
        Element model = document.getRootElement();
        for (Iterator<Element> it = model.elementIterator(); it.hasNext(); ) {
            Element table = it.next();
            if (table.getName().equals("table")) {
                return table.attributeValue("tablename");
            }
        }
        return null;
    }

    private static void executeOrder(String cmd) {
        InputStream in;
        try {
            Process pro = Runtime.getRuntime().exec(cmd);
            pro.waitFor();
            in = pro.getInputStream();
            BufferedReader read = new BufferedReader(new InputStreamReader(in));
            String result = read.readLine();
            System.out.println("INFO:" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Configuration getConfiguration() {
        Configuration conf = HBaseConfiguration.create();
        try {
            HbaseProps hbaseProps = HbaseProps.build();
            conf.set("hbase.zookeeper.quorum", hbaseProps.getQuorum());
            conf.set("hbase.zookeeper.property.clientPort", hbaseProps.getClientPort());
            conf.set("hbase.client.scanner.timeout.period", hbaseProps.getPeriod());
            conf.set("hbase.client.keyvalue.maxsize", hbaseProps.getMaxSize());// 列长度无限
            conf.set("dfs.socket.timeout", hbaseProps.getSocketTimeout());
            conf.set("hbase.security.authentication.tbds.secureid", hbaseProps.getHbase_tbds_secureid());
            conf.set("hbase.security.authentication.tbds.securekey", hbaseProps.getHbase_tbds_securekey());
            conf.set("hadoop.security.authentication.tbds.secureid", hbaseProps.getHadoop_tbds_secureid());
            conf.set("hadoop.security.authentication.tbds.securekey", hbaseProps.getHadoop_tbds_securekey());
            for (String url : hbaseProps.getHadoop()) {
                conf.addResource(new Path(url));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conf;
    }
}
